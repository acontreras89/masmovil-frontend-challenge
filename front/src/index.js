import React from 'react';
import ReactDOM from 'react-dom';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import reducer from './reducer';
import sagas from './sagas';
import App from './App';
import PhoneList from './ui/phoneList/PhoneListEnhanced';
import Phone from './ui/phoneDetails/PhoneEnhanced';
import registerServiceWorker from './registerServiceWorker';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(sagas);

ReactDOM.render((
  <Router>
    <Provider store={store}>
      <App>
        <Route path="/" exact component={PhoneList} />
        <Route path="/phone/:id" exact component={Phone} />
      </App>
    </Provider>
  </Router>
), document.getElementById('app'));

registerServiceWorker();
