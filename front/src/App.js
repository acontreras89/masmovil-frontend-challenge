import React, { Component } from 'react';
import { Route } from 'react-router';
import PropTypes from 'prop-types';
import AppWrapper from './ui/shared/AppWrapper'
import AppHeader from './ui/shared/AppHeader'
import MainWrapper from './ui/shared/MainWrapper'
import MainHeader from './ui/shared/MainHeader'
import MainContents from './ui/shared/MainContents'
import BackButton from './ui/buttons/BackButton'

class App extends Component {
  render() {
    return (
      <AppWrapper>
        <AppHeader />
        <MainWrapper>
          <MainHeader>
            <Route>
              {props => props.location.pathname === '/'
                ? <h1 className="main-title">Smartphones</h1>
                : <BackButton text='< Go back to phone list' onClick={props.history.goBack} />
              }
            </Route>
          </MainHeader>
          <MainContents>
            {this.props.children}
          </MainContents>
        </MainWrapper>
      </AppWrapper>
    );
  }
}

App.propTypes = {
  children: PropTypes.node.isRequired
}

export default App;
