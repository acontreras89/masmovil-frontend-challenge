import { call, apply, put, takeEvery } from 'redux-saga/effects'
import { mergeEntities, update } from 'reduken/entities'
import { hset } from 'reduken/hash'

function* fetchPhones() {
  yield put(hset('isReady', 'phones', false))
  try {
    const response = yield call(fetch, 'http://localhost:3000/phones')
    const data = yield apply(response, 'json')
    const phones = data.reduce(
      (acc, phone) => Object.assign(acc, { [phone.id]: phone })
    , {})
    yield put(mergeEntities({ phones }))
  } catch (error) {
    // TODO: Add proper error management
    console.error('Phone data fetch failed')
  } finally {
    yield put(hset('isReady', 'phones', true))
  }
}

function* fetchPhone(action) {
  const { id } = action.payload
  yield put(hset('isReady', `phones/${id}`, false))
  try {
    const response = yield call(fetch, `http://localhost:3000/phones/${id}`)
    const data = yield apply(response, 'json')
    yield put(update('phones', id, data))
  } catch (error) {
    // TODO: Add proper error management
    console.error('Phone data fetch failed')
  } finally {
    yield put(hset('isReady', `phones/${id}`, true))
  }
}

function* watchFetchPhones() {
  yield takeEvery('FETCH_PHONES', fetchPhones)
  yield takeEvery('FETCH_PHONE', fetchPhone)
}

export default watchFetchPhones
