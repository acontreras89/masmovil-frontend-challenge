import { combineReducers } from 'redux'
import { entities, hash } from 'reduken'

export default combineReducers({ entities, hash })
