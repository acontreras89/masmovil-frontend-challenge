import React from 'react'

const MainHeader = props => {
  return (
    <header className="main-header">
      <div className="wrapper">{props.children}</div>
    </header>
  )
}

export default MainHeader
