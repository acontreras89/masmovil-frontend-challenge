import React from 'react'

const MainWrapper = props => {
  return <main className="main-wrapper">{props.children}</main>
}

export default MainWrapper
