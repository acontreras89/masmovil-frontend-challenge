import React from 'react'

const AppWrapper = props => {
  return <div className="app-wrapper">{props.children}</div>
}

export default AppWrapper
