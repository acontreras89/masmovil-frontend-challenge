import React from 'react'

const MainContents = props => {
  return (
    <header className="main-contents">
      <div className="wrapper">{props.children}</div>
    </header>
  )
}

export default MainContents
