import React from 'react'
import PropTypes from 'prop-types'

const Loader = props => {
  return (
    <div className="loader">
      <p>{props.text}</p>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="100"
        height="100"
        viewBox="0 0 100 100"
      >
        <g className="loader-image">
          <circle cx="80" cy="50" r="10" className="loader-circle c3" />
          <circle cx="50" cy="50" r="10" className="loader-circle c2" />
          <circle cx="20" cy="50" r="10" className="loader-circle c1" />
        </g>
      </svg>
    </div>
  )
}

Loader.propTypes = {
  text: PropTypes.string
}

Loader.defaultProps = {
  text: 'Loading'
}

export default Loader
