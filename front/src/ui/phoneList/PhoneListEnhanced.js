import { compose, lifecycle, branch, renderComponent } from 'recompose'
import { connect } from 'react-redux'
import { getAll } from 'reduken/entities'
import { hget } from 'reduken/hash'
import Loader from '../shared/Loader'
import PhoneList from './PhoneList'

const mapStateToProps = () => {
  const phonesSelector = getAll('phones')
  const isReadySelector = hget('isReady', 'phones')
  return state => ({
    isReady: isReadySelector(state),
    items: phonesSelector(state)
  })
}

const enhance = compose(
  connect(mapStateToProps),
  lifecycle({
    componentWillMount() {
      if (!this.props.isReady) this.props.dispatch({ type: 'FETCH_PHONES' })
    }
  }),
  branch(props => !props.isReady, renderComponent(Loader))
)

export default enhance(PhoneList)
