import React from 'react'
import PropTypes from 'prop-types'
import Phone from './PhoneEnhanced'

const PhoneList = ({ items, buttonText }) => (
  <div className="phone-list">
    {items.map(item => <Phone key={item.id} item={item} />)}
  </div>
)

PhoneList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      DeviceName: PropTypes.string.isRequired,
      Brand: PropTypes.string.isRequired,
      colors: PropTypes.string.isRequired
    })
  ).isRequired
}

export default PhoneList
