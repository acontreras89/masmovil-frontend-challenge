import React from 'react'
import PropTypes from 'prop-types'
import Button from '../buttons/Button'

const Phone = props => (
  <div className="phone-list-item">
    <div className="phone-list-item-wrapper">
      <div className="phone-list-details">
        <p className="phone-title">{props.title}</p>
        <p className="phone-description">{props.description}</p>
        <p className="phone-color">
          <span className="label">Available color:</span>
          <span>{props.color}</span>
        </p>
      </div>
      <div className="phone-image">
        <img src={props.image} alt={props.altText} />
      </div>
      <div className="phone-list-action">
        <Button text={props.buttonText} onClick={props.onClick} />
      </div>
    </div>
  </div>
)

Phone.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

Phone.defaultProps = {
  onClick: function() {}
}

export default Phone
