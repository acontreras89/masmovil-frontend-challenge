import { compose, withProps, withHandlers } from 'recompose'
import { withRouter } from 'react-router-dom'
import Phone from './Phone'

// TODO: Use different images
const enhance = compose(
  withRouter,
  withProps(props => ({
    title: props.item.DeviceName,
    description: props.item.Brand,
    color: props.item.colors,
    buttonText: 'View details',
    image: '/images/phone-sample.jpeg'
  })),
  withHandlers({
    onClick: props => () => props.history.push(`/phone/${props.item.id}`)
  })
)

export default enhance(Phone)
