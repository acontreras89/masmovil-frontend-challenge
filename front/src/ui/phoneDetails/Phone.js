import React from 'react'
import PropTypes from 'prop-types'
import PhoneMainData from '../phoneDetails/PhoneMainData'
import PhoneSecondaryData from '../phoneDetails/PhoneSecondaryData'
import {
  PhoneTitle,
  PhoneDescription,
  PhoneSpec,
  PhoneImage
} from './PhoneElements'

const Phone = props => (
  <div className="phone-details">
    <div className="phone-details-wrapper">
      <PhoneMainData>
        <PhoneTitle text={props.item.DeviceName} />
        <PhoneDescription>
          <p>{props.item.status}</p>
        </PhoneDescription>
      </PhoneMainData>
      <PhoneImage url='/images/phone-sample.jpeg' />
      <PhoneSecondaryData
        sectionTitle='Technical specifications'
      >
        <PhoneSpec
          label='Color'
          value={props.item.colors}
        />
        <PhoneSpec
          label='Brand'
          value={props.item.Brand}
        />
      </PhoneSecondaryData>
    </div>
  </div>
)

Phone.propTypes = {
  item: PropTypes.shape({
    DeviceName: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    colors: PropTypes.string.isRequired,
    Brand: PropTypes.string.isRequired
  }).isRequired
}

export default Phone
