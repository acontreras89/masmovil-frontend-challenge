import { compose, lifecycle, branch, renderComponent } from 'recompose'
import { connect } from 'react-redux'
import { getById } from 'reduken/entities'
import { hget } from 'reduken/hash'
import Loader from '../shared/Loader'
import Phone from './Phone'

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params
  const phoneSelector = getById('phones', id)
  const isListReady = hget('isReady', 'phones')
  const isPhoneReady = hget('isReady', `phones/${id}`)
  return state => ({
    isReady: isListReady(state) || isPhoneReady(state),
    item: phoneSelector(state)
  })
}

const enhance = compose(
  connect(mapStateToProps),
  lifecycle({
    componentWillMount() {
      const id = this.props.match.params.id
      if (!this.props.isReady) {
        this.props.dispatch({ type: 'FETCH_PHONE', payload: { id } })
      }
    }
  }),
  branch(props => !props.isReady, renderComponent(Loader))
)

export default enhance(Phone)
