import React from 'react'
import PropTypes from 'prop-types'

export const PhoneTitle = props => <h1 className="phone-title">{props.text}</h1>

export const PhoneDescription = props => (
  <div className="phone-description">{props.children}</div>
)

export const PhoneImage = props => (
  <div className="phone-image">
    <img src={props.url} alt={props.altText} />
  </div>
)

export const PhoneSpec = props => (
  <p className="phone-spec">
    <span className="label">{props.label}</span>
    <span>{props.value}</span>
  </p>
)

PhoneTitle.propTypes = {
  text: PropTypes.string.isRequired
}

PhoneImage.propTypes = {
  url: PropTypes.string.isRequired,
  altText: PropTypes.string
}

PhoneSpec.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string
}
