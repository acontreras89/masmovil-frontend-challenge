import React from 'react'

const PhoneMainData = props => (
  <div className="phone-main-data">{props.children}</div>
)

export default PhoneMainData
