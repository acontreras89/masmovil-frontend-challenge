import React from 'react'
import PropTypes from 'prop-types'

const PhoneSecondaryData = props => (
  <div className="phone-secondary-data">
    <h2 className="section-title">{props.sectionTitle}</h2>
    {props.children}
  </div>
)

PhoneSecondaryData.propTypes = {
  sectionTitle: PropTypes.string
}

export default PhoneSecondaryData
