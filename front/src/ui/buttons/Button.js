import React from 'react'
import PropTypes from 'prop-types'

const Button = props => (
  <button className="button" onClick={props.onClick}>
    <span className="text">{props.text}</span>
  </button>
)

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string
}

Button.defaultProps = {
  onClick: function() {}
}

export default Button
