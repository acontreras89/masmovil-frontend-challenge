import React from 'react'
import PropTypes from 'prop-types'

const BackButton = props => (
  <button className="back-button" onClick={props.onClick}>
    <span className="text">{props.text}</span>
  </button>
)

BackButton.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string
}

BackButton.defaultProps = {
  onClick: function() {}
}

export default BackButton
