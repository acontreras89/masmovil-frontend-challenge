# másmóvil frontend challenge

This repository consists of two parts:

- A minimal server exposing a REST API to supply phone data
- A web application which consumes the data to display a phone catalog

Both the server and the client are dockerized. To install and deploy them, simply run the following command inside the root directory (where this README is found):

```bash
docker-compose up
```

This exposes the server API in port 3000, and the web application in port 5000.

## Server (`back` folder)

The server is implemented using `json-server`. Phone data can be found in `back/db.json`.

## Client (`front` folder)

The web application has been built using `create-react-app`.
